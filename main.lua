-- Go to love2d.org to install Löve — the code here is the basis of a
-- game engine, not a full game. It’ll hopefully show that it’s not too 
-- hard to make your own roguelike!

ts = 16 -- The size of each drawn tile
tilemap = {
    {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, -- A map is simply a
    {1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,2,2,1}, -- table (like a ‘list’
    {1,0,0,0,1,0,0,0,1,0,0,0,0,1,2,2,2,1}, -- in python) of tables,
    {1,0,0,0,0,0,0,0,0,0,0,1,0,1,0,2,2,1}, -- each representing a
    {1,0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,2,1}, -- row
    {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
}       -- That way, the numbers and the tiles in the map are one to one

map_tiles = {     -- Tables in Lua also work like hashtables (think
    [0] = {color = {0,0,0}, passable = true },  -- Python's dic-
    [1] = {color = {1,1,1}, passable = false},  -- tionaries).
    [2] = {color = {1,0,0}, passable = true }   -- This table
}      -- 'maps' the map numbers to tiles with their own attributes

mobs = {      -- The player is simply another mob. In this table, we
    player = {color = {0,1,0},   x = 2, y = 3}, -- store only
    orc =    {color = {0,0.5,1}, x = 2, y = 5}  -- the x and y
}             -- for each mob, not a whole ‘nother tilemap of mobs

function draw_tile(x, y, entity)      -- This will draw the tile
    love.graphics.setColor(unpack(entity.color))  -- (or mob)
    love.graphics.rectangle('fill', x*ts, y*ts, 1*ts, 1*ts)
end                  -- when it's called in the love.draw() function

function move(mob, dir)    -- The move function need not be diff-
    local poss_moves = {   -- erent between enemy mobs and the
        ['up']    = {mob.x, mob.y - 1},   -- player. Here, we
        ['down']  = {mob.x, mob.y + 1},   -- create a movement
        ['left']  = {mob.x - 1, mob.y},   -- system that can be
        ['right'] = {mob.x + 1, mob.y}    -- used by both. We'll
    }                                     -- deal with the arrow
    new_x, new_y = unpack(poss_moves[dir]) -- keys later
    if map_tiles[tilemap[new_y][new_x]].passable then
        mob.x, mob.y = new_x, new_y
        return true -- Reusable functions are important for
    end  -- consistent code! The next function is meant for the
end -- ”enemy”, but also works for the player mob if used as such

function follow_mob(mob, target)
    if target.x < mob.x then move(mob, 'left')
    elseif target.x > mob.x then move(mob, 'right') end
    if target.y < mob.y then move(mob, 'up')
    elseif target.y > mob.y then move(mob, 'down') end
end

          -- These functions are Löve's "callbacks". They are called
function love.keypressed(key)       -- automatically at certain
    if key == 'up' or key == 'down' -- times. Can you guess when
    or key == 'left' or key == 'right' then
        move(mobs.player, key)      -- this function is called?
    end
end

time = 0        -- The update callback runs many times per second
function love.update(dt)   -- and contains a lot of the timing
    time = time + dt       -- logic. Here we use it to count to
    if time > 1 then       -- one second using "delta time" (dt).
        follow_mob(mobs.orc, mobs.player)
        time = 0   -- Once that second is reached, the enemy mob
    end     -- moves after the player and the timer is reset. Many
end         -- games use libraries to make timing code less... icky

function love.draw() -- After updating, everything is drawn to
    for y, row in pairs(tilemap) do -- the screen, tile-by-tile!
        for x, tile in pairs(row) do
            draw_tile(x, y, map_tiles[tile])
        end
    end
    for _, mob in pairs(mobs) do    -- This game could easily have
        draw_tile(mob.x, mob.y, mob)-- Image support, mob health 
    end -- ...or even actual gameplay! Check out the löve forums and get
end -- coding! also check out my similar game at rendello.ca for how I’ve
-- created these systems, and feel free to email me at my posted address :D
-- happy hacking!                       gaven@rendello.ca
