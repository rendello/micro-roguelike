# Micro Löve Roguelike Engine

The purpose of this project is to make a readable, comprehensible, and
extendable roguelike in less than a standard page's worth of code.

This engine doesn't aim to be great, it aims to make game design less scary :D

![The article-ized code](screenshot.png "The article-ized code")
